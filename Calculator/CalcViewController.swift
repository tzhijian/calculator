import UIKit

class CalcViewController: UIViewController {
    
    var resultLabel: UILabel!
    var btn: UIButton!
    var integer1 = 0
    var integer2 = 0
    var operationSign: String? = ""
    var result = 0
    var isTyping = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resultLabel = UILabel()
        
        resultLabel.frame.origin = CGPoint(x: 10, y: 100)
        resultLabel.frame.size = CGSize(width: self.view.frame.size.width-20, height: 100)
        resultLabel.backgroundColor = UIColor.grayColor()
        resultLabel.text = ""
        resultLabel.font = UIFont.boldSystemFontOfSize(40)
        self.view.addSubview(resultLabel)
        
        var xValue = 0
        var yValue = 0
        for var btnNum = 1; btnNum <= 9; btnNum++ {
            if (btnNum <= 3) {
                xValue = (btnNum-1) * 80 + 10
                yValue = 220
            } else if (btnNum <= 6) {
                xValue = (btnNum-4) * 80 + 10
                yValue = 300
            } else {
                xValue = (btnNum-7) * 80 + 10
                yValue = 380
            }
            var btnFrame: CGRect = CGRect(x: xValue, y: yValue, width: 60, height: 60)
            btn = UIButton(frame: btnFrame)
            btn.setTitle("\(btnNum)", forState: UIControlState.Normal)
            btn.backgroundColor = UIColor.blackColor()
            self.view.addSubview(btn)
            btn.addTarget(self, action: "number:", forControlEvents: UIControlEvents.TouchUpInside)
            NSLog("\(btnNum)")
        }
        
        var num0Frame: CGRect = CGRect(x: 90, y: 460, width: 60, height: 60)
        var num0: UIButton = UIButton(frame: num0Frame)
        num0.setTitle("0", forState: UIControlState.Normal)
        num0.backgroundColor = UIColor.blackColor()
        self.view.addSubview(num0)
        num0.addTarget(self, action: "number:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var additionFrame: CGRect = CGRect(x: 250, y: 220, width: 60, height: 60)
        var addition: UIButton = UIButton(frame: additionFrame)
        addition.setTitle("+", forState: UIControlState.Normal)
        addition.backgroundColor = UIColor.blackColor()
        self.view.addSubview(addition)
        addition.addTarget(self, action: "operation:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var subtractionFrame: CGRect = CGRect(x: 250, y: 300, width: 60, height: 60)
        var subtraction: UIButton = UIButton(frame: subtractionFrame)
        subtraction.setTitle("-", forState: UIControlState.Normal)
        subtraction.backgroundColor = UIColor.blackColor()
        self.view.addSubview(subtraction)
        subtraction.addTarget(self, action: "operation:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var equalFrame: CGRect = CGRect(x: 250, y: 380, width: 60, height: 60)
        var equal: UIButton = UIButton(frame: equalFrame)
        equal.setTitle("=", forState: UIControlState.Normal)
        equal.backgroundColor = UIColor.blackColor()
        self.view.addSubview(equal)
        equal.addTarget(self, action: "equal:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var clearFrame: CGRect = CGRect(x: 250, y: 460, width: 60, height: 60)
        var clear: UIButton = UIButton(frame: clearFrame)
        clear.setTitle("C", forState: UIControlState.Normal)
        clear.backgroundColor = UIColor.blackColor()
        self.view.addSubview(clear)
        clear.addTarget(self, action: "clear:", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.backgroundColor = UIColor.cyanColor()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func number(sender: AnyObject!) {
        if isTyping == true {
        var number:String? = sender.currentTitle
        resultLabel.text = resultLabel.text! + number!
        } else {
            resultLabel.text = ""
            var number:String? = sender.currentTitle
            resultLabel.text = resultLabel.text! + number!
            isTyping = true
        }
    }
    
    func operation(sender: AnyObject!) {
        operationSign = sender.currentTitle
        integer1 = resultLabel.text!.toInt()!
        resultLabel.text = ""
        
        if operationSign == "+" {
            result = integer1 + integer2
        } else if operationSign == "-" {
            result = integer1 - integer2
        }
        isTyping = true
    }
    
    func equal(sender: AnyObject!) {
        integer2 = resultLabel.text!.toInt()!
        
        if operationSign == "+" {
            result = integer1 + integer2
        } else if operationSign == "-" {
            result = integer1 - integer2
        }
        resultLabel.text = "\(result)"
        isTyping = false
    }
    
    func clear(sender: AnyObject!) {
        resultLabel.text = ""
        integer1 = 0
        integer2 = 0
        result = 0
    }
}